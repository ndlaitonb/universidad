package model;

import java.util.ArrayList;

import model.dao.FacultadDao;

public class Universidad {
    // ATRIBUTOS
    private String nombre;
    private String nit;
    private String direccion;
    private ArrayList<String> telefonos;
    private String email;
    private ArrayList<Facultad> facultades;

    // CONSTRUCTOR
    public Universidad(String nombre, String nit, String direccion, String email) {
        this.nombre = nombre;
        this.nit = nit;
        this.direccion = direccion;
        this.email = email;
        this.facultades = new ArrayList<Facultad>();
    }

    // CONSULTORES
    public String getNombre() {
        return nombre;
    }

    public String getNit() {
        return nit;
    }

    public String getDireccion() {
        return direccion;
    }

    public ArrayList<String> getTelefonos() {
        return telefonos;
    }

    public String getEmail() {
        return email;
    }

    // MODIFICADORES
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public void setTelefonos(ArrayList<String> telefonos) {
        this.telefonos = telefonos;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    // ACCIONES
    public void crearFacultad(ConexionDB objConn, String codigo, String nombre) {
        // Crear un objeto de tipo facultad
        FacultadDao objFacultad = new FacultadDao(codigo, nombre);
        objFacultad.insert(objConn, nit);
        facultades.add(objFacultad);
    }

}
