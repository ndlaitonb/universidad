package model.dao;

import java.sql.PreparedStatement;

import model.ConexionDB;
import model.Facultad;

public class FacultadDao extends Facultad {

    public FacultadDao(String codigo, String nombre) {
        super(codigo, nombre);
    }

    // --CONSULTAS SQL
    public void insert(ConexionDB objConn, String nitUniversidad) {
        try {
            if (objConn.getConexion() != null) {
                String query = "INSERT INTO facultades(codigo, nombre, universidad_nit) VALUES(?, ?, ?)";
                // Preparar la consulta
                PreparedStatement pst = objConn.getConexion().prepareStatement(query);
                pst.setString(1, getCodigo());
                pst.setString(2, getNombre());
                pst.setString(3, nitUniversidad);
                // Ejecutar la consulta
                pst.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
