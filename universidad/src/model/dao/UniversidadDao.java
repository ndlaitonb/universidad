package model.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.ConexionDB;
import model.Universidad;

public class UniversidadDao extends Universidad {

    public UniversidadDao(String nombre, String nit, String direccion, String email) {
        super(nombre, nit, direccion, email);
    }

    // --CONSULTAS SQL
    // -------QUERIES-------

    public boolean insert(ConexionDB objConn) {
        boolean insert = false;
        try {
            String query = "INSERT INTO universidades VALUES(?, ?, ?, ?)";
            PreparedStatement pst = objConn.getConexion().prepareStatement(query);
            pst.setString(1, getNit());
            pst.setString(2, getNombre());
            pst.setString(3, getDireccion());
            pst.setString(4, getEmail());
            // Ejecutar consulta
            insert = pst.executeUpdate() == 1 ? true : false;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return insert;
    }

    public static ResultSet selectAll(ConexionDB conn) {
        ResultSet result = null;
        try {
            String query = "SELECT * FROM universidades";
            result = conn.getConexion().createStatement().executeQuery(query);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public ResultSet selectXnit(ConexionDB conn) {
        ResultSet result = null;
        try {
            String query = "SELECT * FROM universidades WHERE nit = ?";
            PreparedStatement pst = conn.getConexion().prepareStatement(query);
            pst.setString(1, getNit());
            result = pst.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static ResultSet selectXnit(ConexionDB conn, String nit) {
        ResultSet result = null;
        try {
            String query = "SELECT * FROM universidades WHERE nit = ?";
            PreparedStatement pst = conn.getConexion().prepareStatement(query);
            pst.setString(1, nit);
            result = pst.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean updateByNit(ConexionDB objConn, String nit, String nombre, String direccion, String email) {
        boolean update = false;

        try {
            String query = "UPDATE universidades SET nombre=?, direccion=?, email=? WHERE nit = ?";
            PreparedStatement pst = objConn.getConexion().prepareStatement(query);
            pst.setString(1, nombre);
            pst.setString(2, direccion);
            pst.setString(3, email);
            pst.setString(4, nit);
            // Ejecutar consulta
            update = pst.executeUpdate() == 1 ? true : false;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return update;
    }

    public static boolean deleteByNit(ConexionDB objConn, String nit) {
        boolean delete = false;
        try {
            String query = "DELETE FROM universidades WHERE nit = ?";
            PreparedStatement pst = objConn.getConexion().prepareStatement(query);
            pst.setString(1, nit);
            // Ejecutar
            delete = pst.executeUpdate() == 1 ? true : false;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return delete;
    }

}
