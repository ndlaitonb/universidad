package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConexionDB {
    // ATRIBUTO
    private Connection conexion;

    // CONSTRUCTOR
    public ConexionDB() {
        try {
            conexion = DriverManager.getConnection("jdbc:sqlite:universidad_grupo14");
            if (conexion != null) {
                System.out.println("Conexión realizada con éxito a la BD");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // CONSULTOR
    public Connection getConexion() {
        return conexion;
    }

    // ACCIONES
    public void cerrarConexion() throws SQLException {
        conexion.close();
    }
}
