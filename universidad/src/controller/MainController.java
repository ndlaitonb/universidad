package controller;

import model.ConexionDB;
import view.Menu;
import view.MenuGUI;

public class MainController {

    public MainController() {
        // CREAR OBJETOS
        ConexionDB conexionDB = new ConexionDB();
        UniversidadController uController = new UniversidadController(conexionDB);
        // Menu menu = new Menu(uController);
        // menu.crearMenu();
        MenuGUI menuGui = new MenuGUI(uController);
        menuGui.crearMenu();
        // Cerrar conexión
        try {
            conexionDB.cerrarConexion();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
