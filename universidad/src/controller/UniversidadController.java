package controller;

import java.sql.ResultSet;
import java.util.ArrayList;

import model.ConexionDB;
import model.dao.UniversidadDao;

public class UniversidadController {
    // ATRIBUTO
    private ConexionDB conexionDB;
    private ArrayList<UniversidadDao> universidades;

    public UniversidadController(ConexionDB conexionDB) {
        this.conexionDB = conexionDB;
        this.universidades = new ArrayList<UniversidadDao>();
    }

    // ACCIONES
    public boolean crearUniversidad(String nit, String nombre, String direccion, String email) {
        UniversidadDao universidad = new UniversidadDao(nombre, nit, direccion, email);
        boolean insert = universidad.insert(conexionDB);
        if (insert) {
            this.universidades.add(universidad);
        }
        return insert;
    }

    public ResultSet obtenerUniversidades() {
        return UniversidadDao.selectAll(conexionDB);
    }

    public ResultSet consultarUniversidad(String nit) {
        return UniversidadDao.selectXnit(conexionDB, nit);
    }

    public boolean actualizarUniversidad(String nit, String nombre, String direccion, String email) {
        return UniversidadDao.updateByNit(conexionDB, nit, nombre, direccion, email);
    }

    public boolean eliminarUniversidad(String nit) {
        return UniversidadDao.deleteByNit(conexionDB, nit);
    }

}
