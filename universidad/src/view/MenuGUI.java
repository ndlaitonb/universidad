package view;

import javax.swing.JOptionPane;

import controller.UniversidadController;

public class MenuGUI {
    // ATRIBUTOS
    private UniversidadController uController;

    // CONSTRUCTOR
    public MenuGUI(UniversidadController uController) {
        this.uController = uController;
    }

    public void crearMenu() {

        UniversidadView uView = new UniversidadView(uController);
        // Opciones del menú
        String mensaje = "-----------------UNIVERSIDADES----------------\n";
        mensaje += "1) Crear universidad\n";
        mensaje += "2) Mostrar todas las univerisdades\n";
        mensaje += "3) Consultar universidad\n";
        mensaje += "4) Actualizar universidad\n";
        mensaje += "5) Eliminar universidad\n";
        mensaje += "-1) Salir\n";
        mensaje += ">>> ";
        // Varibale que representa la opción ingresada por el usuario
        int opcion = 0;
        try {
            while (opcion != -1) {
                System.out.print(mensaje);
                opcion = Integer.parseInt(JOptionPane.showInputDialog(null, mensaje));
                // Evaluar opción
                /*
                 * switch (opcion) {
                 * case 1:
                 * uView.crearUniversidad(sc);
                 * break;
                 * case 2:
                 * uView.mostrarUniversidades();
                 * break;
                 * case 3:
                 * uView.mostrarUnviersidadXnit(sc);
                 * break;
                 * case 4:
                 * uView.actualizarUniversidad(sc);
                 * break;
                 * case 5:
                 * uView.eliminarUniversidad(sc);
                 * break;
                 * default:
                 * break;
                 * }
                 */
            }
        } catch (Exception e) {
            // TODO: handle exception
        }
    }

}
